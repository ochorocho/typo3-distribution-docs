# DDEV - local web development environment

DDEV is used in the context of the TYPO3 Project Template to
provide a containerized environment to run TYPO3 on almost
any Operating System (Windows, Linux, Mac and even GitPod).

The template provides a custom command (`ddev typo3-init`) that will start DDEV,
install all dependencies ([ddev add-ons](https://ddev.readthedocs.io/en/stable/users/extend/additional-services/), [composer](https://packagist.org/) and [node packages](https://www.npmjs.com/)),
setup TYPO3 using the `./vendor/bin/typo3 setup` command.

## Commands

Prepare TYPO3 after the repository was cloned:

```bash
ddev typo3-init
```

## More information about DDEV

 * [Get Started](https://ddev.readthedocs.io/en/stable/)
 * [Custom configuration](https://ddev.readthedocs.io/en/stable/users/configuration/config/) - Configure the webserver, PHP version or Database system to be used 
 * [Step Debugging with Xdebug](https://ddev.readthedocs.io/en/stable/users/debugging-profiling/step-debugging/) - Go through your code step by step to analyze and find issues
 * [Hosting Provider Integration](https://ddev.readthedocs.io/en/stable/users/providers/) - Update your local environment (Database and Files)
