# Vite Frontend Tooling

Vite provides a simple yet powerful way to process/bundles
all assets for your site. Vite is easy to configure and amazingly fast.

With the [Vite Asset Collector](https://packagist.org/packages/praetorius/vite-asset-collector) it
is tightly integrated in TYPO3.

What it does out of the box:
 * Compress JavaScript and create chunks (`assets/JavaScript`)
 * Transpile and compress SCSS (`assets/Scss`)
 * Compress images (`assets/Image`) and fonts (`assets/Fonts`) to the Public folder of EXT:site-distribution

## Commands

Build for development:

```bash
ddev npm run build:development
```

Build for production:

```bash
ddev npm run build:production
```

Compile on file changes:

```bash
ddev npm run watch
```

## Vite Dev Server - Hot Module Replacement ([HMR](https://vitejs.dev/guide/features.html#hot-module-replacement))

Automatically update the website on file changes.

Run the Vite dev server in foreground:

```bash
ddev vite
```

Don't forget to flush the fronend caches so the URLs get updated to target the dev server.

## More information about Vite

* [Get Started](https://vitejs.dev/guide/)
* [Configuration](https://vitejs.dev/config/) - Configure the build toolchain, add custom plugins
* [DDEV Vite Add-on](https://github.com/s2b/ddev-vite-sidecar)
