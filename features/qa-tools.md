# QA / Analysis

## Run PHPStan

[PHPStan](https://phpstan.org/user-guide/getting-started) is a static analysis tool for PHP code that helps identify and report potential
errors, bugs, and inconsistencies in the codebase without actually executing the code.

```bash
ddev exec ./vendor/bin/phpstan analyse -c .phpstan.neon --no-progress
```

## PHP CS Fixer

[PHP CS Fixer](https://cs.symfony.com/) is a command-line tool for automatically fixing coding style issues in PHP code according to
specified coding standards and rules.

```bash
ddev exec ./vendor/bin/php-cs-fixer fix --dry-run --diff
```
