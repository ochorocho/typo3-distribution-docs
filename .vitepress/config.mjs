import { fileURLToPath, URL } from 'node:url'
import {defineConfig} from 'vitepress'
const base = "/typo3-distribution-docs/"

export default defineConfig({
    title: "TYPO3 Project Template",
    description: "How to extend the TYPO3 Project Template",
    outDir: 'dist',
    base: `${base}`,
    head: [
        ['link', { rel: "shortcut icon", href: "/typo3-distribution-docs/favicon.ico"}],
        ['link', { rel: "shortcut icon", href: "/typo3-distribution-docs/typo3-logo.svg"}],
    ],
    markdown: {
        lineNumbers: true,
        config: (md) => {
            // Render "```mermaid"
            const defaultRenderer = md.renderer.rules.fence.bind(md.renderer.rules);
            md.renderer.rules.fence = (tokens, idx, options, env, instance) => {
                if(tokens[idx].info === 'mermaid') {
                    return `<ClientOnly><Typo3Mermaid>${tokens[idx].content}</Typo3Mermaid></ClientOnly>`;
                }

                // Return default renderer, so all other rules still apply
                return defaultRenderer(tokens, idx, options, env, instance);
            };
        }
    },
    themeConfig: {
        logo: '/typo3-logo.svg',
        nav: [
            {text: 'Home', link: '/'},
        ],
        editLink: {
            pattern: 'https://gitlab.com/ochorocho/typo3-distribution-docs/-/edit/main/:path'
        },
        search: {
            provider: 'local'
        },
        sidebar: [
            {
                text: 'Introduction',
                items: [
                    { text: 'Getting Started', link: '/getting-started' },
                    { text: 'Overview', link: '/visual-overview' },
                    { text: 'Changelog', link: '/changelog' },
                ]
            },
            {
                text: 'Tools',
                items: [
                    { text: 'DDEV', link: '/features/ddev-stack' },
                    { text: 'Vite', link: '/features/vite-build' },
                    { text: 'Deployer (PHP)', link: '/features/php-deployer' },
                ]
            },
            {
                text: 'Frontend',
                items: [
                    { text: 'Lit - Web Components', link: '/frontend/lit-web-component' },
                    { text: 'VueJS', link: '/frontend/vue-js-framework' },
                    { text: 'TypeScript', link: '/frontend/typescript' }
                ]
            }
        ],
        socialLinks: [
            {
                icon: `${base}gitlab-logo.svg`,
                ariaLabel: 'TYPO3 Template on GitLab',
                link: 'https://gitlab.com/gitlab-org/project-templates/typo3-distribution/',
            }
        ],
        // TYPO3 theme colors
        color: {
            white: '#ffffff',
            orange: '#f49700',
            orangeDark: '#e18d00',
            orangeLight: '#f7a831',
            gray: '#515151',
            grayDark: '#8c8c8c',
            grayLight: '#b9b9b9',
            graySuperLight: '#f4f4f4',
            valid: '#5abc55',
            error: '#dd123d',
            warning: '#ffc800',
            information: '#0080ff',
        }
    },
    vite: {
        build: {
            // As of now suppress the limit warning introduced by mermaids flowchart-elk-definition.
            chunkSizeWarningLimit: 1600
        },
        resolve: {
            alias: [
                {
                    // Override the default VPSocialLink link
                    find: /^.*\/VPSocialLink\.vue$/,
                    replacement: fileURLToPath(
                        new URL('./components/Typo3SocialLink.vue', import.meta.url)
                    )
                },
                {
                    // Override the default VPSocialLink link
                    find: /^.*\/VPHome\.vue$/,
                    replacement: fileURLToPath(
                        new URL('./theme/Typo3Home.vue', import.meta.url)
                    )
                }
            ],
        },
    },
    vue: {
        template: {
            compilerOptions: {
                whitespace: 'preserve'
            },
        },
    }
})
