# Deployer - Push code to the server

Deployer is a deployment automation tool that comes with loads of pre-defined
deployment scripts, called [recipes](https://deployer.org/docs/7.x/recipe).

It's recommended to run [deployer](https://deployer.org/)
in GitLab CI. Using the CI allows you to do pre-flight checks and testing prior to
pushing the code to the server.

## Related files

* The Deployer recipe describes the general process of the application deployment: [packages/site-distribution/Configuration/DeployerRsync.php](https://gitlab.com/gitlab-org/project-templates/typo3-distribution/-/blob/main/packages/site-distribution/Configuration/DeployerRsync.php?ref_type=heads)
  (no need to touch this file unless you need to implement some extraordinary logic)
* Project specific configuration: [deploy.yaml](https://gitlab.com/gitlab-org/project-templates/typo3-distribution/-/blob/main/deploy.yaml)
* Defines jobs to run in the CI: [.gitlab-ci.yml](https://gitlab.com/gitlab-org/project-templates/typo3-distribution/-/blob/main/.gitlab-ci.yml#L78-104)

## Configuration

To configure deployment for the TYPO3 Template

1. Edit `deploy.yaml` and define a `hostname`, `remote_user` and the `deploy_path` for both, staging and production
2. [Generate](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair) an SSH key pair using
   `ssh-keygen -t ed25519 -C "my project" -f typo3-template`. This will generate 2 files in the current directory:
   `typo3-template.pub` (public key), `typo3-template` (private key - **keep it secret!**)
3. Add the generated private key as variable `SSH_PRIVATE_KEY` in Settings -> CI/CI -> Variables of the project
4. [Enable deployer](https://gitlab.com/gitlab-org/project-templates/typo3-distribution/-/blob/main/.gitlab-ci.yml?ref_type=heads#L78-104)
   in the `.gitlab-ci.yml` file by uncommenting `./vendor/bin/dep deploy ...` for both jobs (`deploy_to_staging`, `deploy_to_production`)

The `deploy.yaml` comes with 2 pre-defined deployment hosts (staging, production).

Example:

```yaml
hosts:
  staging:
    hostname: staging.typo3.org
    remote_user: ochorocho_staging
    deploy_path: '/var/www/html/'
    rsync_src: './'
    ssh_multiplexing: false
    php_version: '8.2'
  production:
    hostname: production.typo3.org
    remote_user: ochorocho_production
    deploy_path: '/var/www/html/'
    rsync_src: './'
    ssh_multiplexing: false
    php_version: '8.2'
```

GitLab CI Example (`.gitlab-ci.yml`):

```yaml
deploy_to_staging:
  <<: *add_ssh
  environment:
    name: staging
    url: https://your-staging-domain.com
  script:
    - ./vendor/bin/dep deploy -vvv staging
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'
      when: on_success

deploy_to_production:
  <<: *add_ssh
  environment:
    name: production
    url: https://your-domain.com
  script:
    - ./vendor/bin/dep deploy production
  rules:
    - if: '$CI_COMMIT_TAG'
      when: on_success
```

After the first deployment the environments show up in GitLab under
Operate -> Environments. This gives a good overview of what is currently
deployed on staging or production environments.

![GitLab environments](../assets/gitlab-environments.png)

## GitLab pipeline

```mermaid
flowchart LR

    subgraph Build["Build"]
        direction RL
        Assets["Install packages & generate assets"]
        Install["Install composer packages"]
    end

    subgraph Testing["Test"]
        direction RL
        CodeStyle["Code style (PHP CS Fixer)"]
        StaticAnalysis["Static analysis (PHPStan)"]
    end

    subgraph Deploy["Deploy"]
        direction RL
        DeployToTarget["Deploy to target (only on success)"]
    end

    Build --> Testing
    Testing --> Deploy
```

## Additional deployment target

To add an additional deployment target, extend the `hosts:` section in the `deploy.yaml` file
and add another job for the new target in `.gitlab-ci.yml`. Change the `script:` command
to use the new deployer target created in the `hosts:` section. Don't forget to adjust `rules:` section
to your needs. You may want to deploy the new target only on a certain branch name.

## Run deployer locally (only for testing):

It might be useful to run deployer locally for testing purposes.

```bash
ddev exec ./vendor/bin/dep deploy -vvv staging
```

::: warning
This will deploy the code in the current directory. So make sure the assets are up-to-date.
:::

## More information about Deployer

* [Get Started](https://deployer.org/docs/7.x/getting-started)
* [Recipe list](https://deployer.org/docs/7.x/recipe)
