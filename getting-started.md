# Getting Started

## Local prerequisites

Only a **Docker provider** and **DDEV** needs to be installed locally on your OS.
Everything else will run within the container.

1. [Docker, Colima, OrbStack or Rancher](https://ddev.readthedocs.io/en/latest/users/install/docker-installation/) - The container engine used by DDEV. 
   **Docker is recommended**, but other containers engines should work as well.
2. [DDEV](https://ddev.readthedocs.io/en/latest/users/install/ddev-installation/)

## Initial setup

### Create a Project

1. Go to your favorite GitLab instance (e.g. gitlab.com)
2. Click **+** > **New project/repository**
3. Scroll down to "TYPO3 Distribution" and click on "Use template"
4. Wait for the project import to finish

<Typo3Video src="videos/gitlab-create-project-from-template.mp4" />

### Download and set up the project locally

1. Copy the git url
2. Clone the repository `git clone <git url>`
3. Go into the folder
4. Run `ddev typo3-init` to install composer/node dependencies and set up TYPO3

Once this the command executed successfully, ddev will launch
the default browser with the TYPO3 backend.

**Default user:**

* Username: `admin`
* Password: `Password.1`

<Typo3Video src="videos/typo3-initialize-project.mp4" />
