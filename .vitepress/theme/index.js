// https://vitepress.dev/guide/custom-theme
import { h } from 'vue'
import Theme from 'vitepress/theme'
import './style.css'
import Typo3Mermaid from '../components/Typo3Mermaid.vue'
import Typo3Video from '../components/Typo3Video.vue'
import Typo3Contributors from '../components/Typo3Contributors.vue'
import Typo3Changelog from '../components/Typo3Changelog.vue'
export default {
  extends: Theme,
  Layout: () => {
    return h(Theme.Layout, null, {
      // https://vitepress.dev/guide/extending-default-theme#layout-slots
    })
  },

  enhanceApp({ app, router, siteData }) {
    app.component('Typo3Mermaid', Typo3Mermaid);
    app.component('Typo3Video', Typo3Video);
    app.component('Typo3Contributors', Typo3Contributors);
    app.component('Typo3Changelog', Typo3Changelog);
  }
}
