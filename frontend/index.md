# How to implement frameworks


| Framework                                    | Description                                                                           | Guide                             |
|----------------------------------------------|---------------------------------------------------------------------------------------|-----------------------------------|
| ![Lit Element](../assets/lit-logo.svg)       | Simple. Fast. Web Components.                                                         | [integrate](lit-web-component.md) |
| ![VueJs](../assets/vue-logo.svg)             | An approachable, performant and versatile framework for building web user interfaces. | [integrate](vue-js-framework.md)  |
| ![TypeScript](../assets/typescript-logo.svg) | JavaScript with syntax for types.                                                     | [integrate](typescript.md)        |
