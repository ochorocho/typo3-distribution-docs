# Add Lit Element to the frontend

[Lit Element](https://lit.dev/) is a lightweight library for creating Web Components.

## Install `lit`

```bash
ddev npm install lit
```

## Create `JavaScript/my-component.js`

```js
import {html, css, LitElement} from 'lit';

export class MyComponent extends LitElement {
    static styles = css`
      .my-component {
        border: 2px solid #ff0000;
      }
    `;

    static get properties() {
        return {
            uid: { type: Number },
            title: { type: String },
        };
    }

    render() {
        return html`<div class="my-component">${this.title} [uid:${this.uid}]</div>`;
    }
}

customElements.define('my-component', MyComponent);
```

##  Add it to the `assets/app.js` entrypoint

```js
import "./Scss/app.scss";
import "./JavaScript/made-with.js";
import "./JavaScript/my-component.js"; // [!code ++]

// TYPO3.lang contains all available inline labels
console.log("Say hi! " + TYPO3.lang["site-distribution.welcome"]);
```

## Add the Web Component to your template

```html
<my-component title="My first WebComponent" uid="123"></my-component>
```

## More information about Lit

* [What is Lit?](https://lit.dev/docs/)
* [Get Started](https://lit.dev/docs/getting-started/)
