---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "GitLab Project Template for TYPO3"
  text: ""
  tagline: A quick and easy way to start a new TYPO3 site
  image:
    src: '/typo3-logo.svg'
    alt: VitePress
  actions:
    - theme: brand
      text: Getting started
      link: /getting-started
    - theme: alt
      text: Framework integration
      link: /frontend/index
    - theme: alt
      text: QA Tools
      link: /features/qa-tools

features:
  - title: DDEV
    details: Local development environment (Webserver, PHP, Database + additional services)
    link: features/ddev-stack
  - title: Vite
    details: Frontend build toolchain to process/bundle assets (JavaScript/TypeScript, (S)CSS, Images)
    link: features/vite-build
  - title: Deployer (PHP)
    details: The PHP deployment tool with support for TYPO3 and many more.
    link: features/php-deployer
---
