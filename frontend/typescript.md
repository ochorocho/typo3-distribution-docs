# Add TypeScript support

To enable TypeScript in your project you have to follow a few steps.

## Install node packages

Two additional packages a required for type checking with vite

```bash
ddev npm install vite-plugin-checker @types/stylelint 
```

## Change JavaScript files

* Rename all `*.js` files in the assets folder to `*.ts`
* Change the imports form `import "./JavaScript/made-with.js"` to `import "./JavaScript/made-with"`

## Configure TypeScript

Create the `tsconfig.json` file in the root of the project with the following content:

```json 
{
	"compilerOptions": {
		"target": "es2020",
		"allowSyntheticDefaultImports": true,
		"incremental": true,
		"tsBuildInfoFile": "./.cache/tsconfig.tsbuildinfo.json",
		"module": "es2020",
		// "bundler" required to work with rollup/parseAst types
		"moduleResolution": "bundler",
		"sourceMap": false,
		"removeComments": false,
		"alwaysStrict": true,
		"downlevelIteration": true,
		"experimentalDecorators": true,
		"useDefineForClassFields": false,
		"noImplicitAny": true,
		"noImplicitThis": true,
		"noImplicitReturns": true,
		"pretty": true,
		"types": [
			"vite/client",
			"node"
		],
		"typeRoots": [
			"node_modules/@types",
			"assets/JavaScript/types",
			"./node_modules"
		]
	}
}
```

## Declare TYPO3 specific types

Create the file `assets/JavaScript/types/TYPO3/index.d.ts` with
the following content:

```typescript
declare namespace TYPO3 {
  export const lang: {
    [key: string]: string
  };
  export namespace settings {
    export const ajaxUrls: {
      [key: string]: string
    };
  }
}
```

## Configure Vite

Modify the `vite.config.ts` configuration file

```typescript
import {defineConfig} from 'vite';
import { dirname, resolve } from 'node:path';
import { fileURLToPath } from 'node:url';
import autoprefixer from "autoprefixer";
import { ViteImageOptimizer } from 'vite-plugin-image-optimizer';
import autoOrigin from 'vite-plugin-auto-origin';
import checker from "vite-plugin-checker"; // [!code ++]

// TYPO3 root path (relative to this config file)
const VITE_TYPO3_ROOT = "./";

// Vite input files (relative to TYPO3 root path)
const VITE_ENTRYPOINTS = [
  "assets/app.ts",
  "assets/Scss/rte.scss",
];

// Output path for generated assets
const VITE_OUTPUT_PATH = "packages/site-distribution/Resources/Public/";

const currentDir = dirname(fileURLToPath(import.meta.url));
const rootPath = resolve(currentDir, VITE_TYPO3_ROOT);
export default defineConfig(({command, mode}) => {
  return {
    mode: `${mode}`,
    base: '',
    build: {
      minify: mode !== 'development',
      manifest: true,
      assetsInlineLimit: 100, // Do not inline SVG files, so it can be used by the SvgIconProvider
      rollupOptions: {
        input: VITE_ENTRYPOINTS.map(entry => resolve(rootPath, entry)),
        output: {
          manualChunks: (path) => path.split('/').reverse()[path.split('/').reverse().indexOf('node_modules') - 1]
        }
      },
      outDir: resolve(rootPath, VITE_OUTPUT_PATH),
    },
    publicDir: false,
    plugins: [
      checker({             // [!code ++]
        typescript: true,   // [!code ++]
      }),                   // [!code ++]
      ViteImageOptimizer(),
      autoOrigin(),
    ],
    css: {
      devSourcemap: true,
      postcss: {
        plugins: [
          autoprefixer({})
        ],
      },
      preprocessorOptions: {
        scss: {
          additionalData: `$mode: ${mode};`,
        },
      },
    },
  }
});
```

## Include app.ts using the Vite Asset Collector

Modify the `packages/site-distribution/Resources/Private/Templates/Default.html` template

```html
<html
    data-namespace-typo3-fluid="true"
    xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers"
    xmlns:vac="http://typo3.org/ns/Praetorius/ViteAssetCollector/ViewHelpers"
>

<f:layout name="{f:if(condition: '{layout}', then: '{layout}', else: 'Default')}"/>

<f:section name="Main">
    <main role="main">
        <f:format.html parseFuncTSPath="lib.parseFunc">{normal}</f:format.html>
    </main>
</f:section>

<f:section name="FooterAssets">
    <vac:asset.vite entry="assets/app.js"/> // [!code --]
    <vac:asset.vite entry="assets/app.ts"/> // [!code ++]
</f:section>

</html>
```


That's it! You are good to go.

Try to run the build
```
ddev npm run build:production
```

## More information about TypeScript

* [Get Started](https://www.typescriptlang.org/docs/)
* [Handbook](https://www.typescriptlang.org/docs/handbook/intro.html)
