# Add VueJS to the frontend

[VueJS](https://vuejs.org/) is a versatile framework for building web user interfaces

## Install `vue` and `@vitejs/plugin-vue`

```bash
ddev npm install vue @vitejs/plugin-vue
```

## Add the plugin to `vite.config.js`

```js
// ...
import vue from '@vitejs/plugin-vue' // [!code ++]

export default defineConfig(({command, mode}) => {
  return {
    // ...
    plugins: [
      ViteImageOptimizer(),
      autoOrigin(),
      vue(), // [!code ++]
    ],
    // ...
  }
})
```

## Add it to the `assets/app.js` entrypoint

```js
// Scss entrypoint
import "./Scss/app.scss";
import "./JavaScript/made-with.js";
import { createApp } from 'vue' // [!code ++]
import App from './JavaScript/App.vue' // [!code ++]

const app = createApp(App) // [!code ++]
app.mount('#app') // [!code ++]

// TYPO3.lang contains all available inline labels
console.log("Say hi! " + TYPO3.lang["site-distribution.welcome"]);

```

## Create the App `assets/JavaScript/App.vue`

```vue
<script>
export default {
    data: () => ({
        counter: 0
    }),
    components: {
      // Import other components
    },
    methods: {
        incrementCounter() {
            this.counter += 1
        }
    }
}
</script>

<template>
    <h1>Hello from App.vue with counter </h1>
    <button @click="incrementCounter">Increment</button>
    -- {{counter}} --
</template>

<style scoped>
    h1 {
        color: #ff0000;
    }
</style>
```

## Add div to mount the app

Add an element to the html template for the Vue app to be mounted on

```html
<div id="app"></div>
```

## More information about VueJS

* [Get Started](https://vuejs.org/guide/introduction.html)
* [VueJS integration in Vite](https://github.com/vitejs/vite-plugin-vue/tree/main/packages/plugin-vue)

