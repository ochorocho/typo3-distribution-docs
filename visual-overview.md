# Visual overview

This diagramm describes the development workflow:

```mermaid
  flowchart TD
    A[Local Development] -->|push code| B{GitLab/Git}
    B --> C[CI/CD - GitLab Runner]
    C -->|"Default branch (main) - automatically"| E(Deploy Staging)
    C -->|"Git tag (e.g. v1.0.0) - manually"| F(Deploy Live)
```

## Local Development

Develop a feature, fix a bug in a dedicated branch (`git checkout -b <branch-name>`). Commit the changes
and push (`git push <branch-name>`) it to GitLab. 

Keep an eye on "[Closing issues automatically](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)"
section in the GitLab docs it may help to manage issues automatically.

## GitLab/Git

GitLab will receive the changes and will display a banner on top so a [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
can be created with a single click.

For Staging deployment create a Merge Request from your branch to the "Default Branch" - `main` if not configured otherwise. 
Once the "Default Branch" is tested and ready, go to Code -> Tags -> New tag and enter a Tag name like `v1.0.0`.

If you prefer to create a tag locally and push it to the server has the same effect:

1. Create a tag: `git tag -a v1.0.0 -m "Version 1.0.0"`
2. Push a tag: `git push origin v1.0.0`

::: tip
A Release can be used as well (Deploy -> Releases -> New release). It behaves the same way, because it 
creates a tag but offers more options like adding links, milestones and release notes.
:::

## CI/CD - GitLab Runner

The GitLab Runner will ask GitLab for Pipelines to process. If there are any
Pipelines the Runner will pick up and process all the Jobs within the Pipeline
according to the definition in [.gitlab-ci.yml](https://gitlab.com/gitlab-org/project-templates/typo3-distribution/-/blob/main/.gitlab-ci.yml).

When all Jobs prior to the `deploy_to_*` job ran successfully the Runner
will push the code to either **Staging** (Default branch - `main`) or **Live** (Git Tag).
